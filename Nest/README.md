
## Description

[Nest] API rest - CRUD Productos. Corre en el puerto 3001.

## Instalación

```bash
$ npm install
```

## Correr apliación

```bash
# Develop
$ npm run start
```

## Rutas

GET /products/stock -->
Devuelve el listado de los productos que estan vigentes y hay en existencia
     
GET /products/empty --> Devuelve el listado de los productos que estan vigentes y no hay en existencia
    
POST /products --> Inserta un nuevo Producto
    
PUT /products/:id --> Actualiza un Producto
    
DELETE /products/:id --> Elimina logicamente un Producto