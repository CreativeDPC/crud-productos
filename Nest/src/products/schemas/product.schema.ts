import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as moment from 'moment';
import { Category } from '../enum/category.enum';

export type ProductDocument = Product & Document;

@Schema()
export class Product {

  @Prop({
      required:true,
      maxlength:150
  })
  nameProduct: string;

  @Prop({
      required:true,
      enum: Category
  })
  category: string;

  @Prop({
      required:true,
      maxlength:450
  })
  description: string;

  @Prop({
      required:true,
      min:0,
      max:100
  })
  productQuantity: number;
  
  @Prop({
      required:true
  })
  status: boolean;
  
  @Prop({
    required:true,
    default: moment().format('YYYY/MM/DD HH:mm:ss')
  })
  timeStamp: Date;
}

export const ProductSchema = SchemaFactory.createForClass(Product);