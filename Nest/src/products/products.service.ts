import { CreateProductDto } from './dto/create-product';
import { Product, ProductDocument } from './schemas/product.schema';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class ProductsService {
    constructor(
        @InjectModel(Product.name) private readonly productModel: Model<ProductDocument>,
      ) {}
    
      async create(newProduct: CreateProductDto): Promise<Product> {
        const createdCat = new this.productModel(newProduct);
        return createdCat.save();
      }
    
      async findAll(): Promise<Product[]> {
        return this.productModel.find({status:true, productQuantity: {$gt:0}}).exec();
      }

      async findAllEmpty(): Promise<Product[]> {
        return this.productModel.find({productQuantity:0,status:true}).exec();
      }

      async update(id:string, product: CreateProductDto | any): Promise<any> {
        return await this.productModel.updateOne(
            { _id: {$eq: id}}, 
            {$set: product});
      }

      async delete(id:string): Promise<any> {
        return await this.productModel.deleteOne({_id: id});
      }
}
