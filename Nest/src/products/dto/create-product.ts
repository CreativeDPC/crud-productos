import { ApiProperty } from "@nestjs/swagger";
import { Category } from "../enum/category.enum";

export class CreateProductDto {
    @ApiProperty()
    readonly nameProduct: string;
    @ApiProperty({enum: Category})
    readonly category: string;
    @ApiProperty()
    readonly description: string;
    @ApiProperty()
    readonly productQuantity: number;
    @ApiProperty()
    readonly status: boolean;
  }