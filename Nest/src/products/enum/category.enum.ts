export enum Category {
    Bebidas = 'Bebidas',
    Limpieza = 'Limpieza',
    Botanas = 'Botanas',
    Cremeria = 'Cremeria',
    Panaderia = 'Panaderia'
  }