import { Product } from './schemas/product.schema';
import { CreateProductDto } from './dto/create-product';
import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put } from '@nestjs/common';
import { ProductsService } from './products.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('products')
@Controller('products')
export class ProductsController {

    constructor(private readonly productService: ProductsService) {}

    @Get('stock')
    async findAll(): Promise<Product[]> {
        try {
            return this.productService.findAll();
        } catch (error) {
            this.exception(error);
        }
        
    }

    @Get('empty')
    async findAllEmpty(): Promise<Product[]> {
        try {
            return this.productService.findAllEmpty();    
        } catch (error) {
            this.exception(error);
        }
    }
    
    @Post()
    async create(@Body() newProduct: CreateProductDto) {
        try {
            await this.productService.create(newProduct); 
        } catch (error) {
            this.exception(error);
        }
    }

    @Put(':id')
    async Update(@Param('id') id: string, @Body() product: CreateProductDto){
        try {
            const result = await this.productService.update(id, product);
            return {
                updatedItems: result.nModified
            }
        } catch (error) {
            this.exception(error);
        }
    }

    @Delete(':id')
    async Delete(@Param('id') id: string){
        try {
            const result = await this.productService.update(id, {status: false});
            return {
                deleteProcces: true
            }
        } catch (error) {
            this.exception(error);
        }
    }    

    private exception(ex) {
        throw new HttpException(ex, HttpStatus.BAD_REQUEST);
    }
}
