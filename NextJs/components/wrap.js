import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Navigation from './navigation';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) => ({
    title:{
        textAlign:'center'
    }
  }));

export default function Wrap(props) {
    const classes = useStyles();
  return (
    <Container maxWidth="xl" disableGutters={true}>
        <Navigation />
        <Grid container spacing={0} direction="row"
              justify="center"
              alignItems="center">
            <Grid item sm={12} md={8}>
              <Box my={2}>
                  <Typography variant="h4" component="h1" className={classes.title}>
                      {props.title}
                  </Typography>
              </Box>
              {props.children}
            </Grid>
         </Grid>
    </Container>
  );
}
