import React, { useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import * as Petition from '../common/petitions-http';
import Button from '@material-ui/core/Button';
import {Product} from '../common/product.class';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';

const useStyles = makeStyles((theme) => ({
    root: {
      '& > *': {
        margin: theme.spacing(1),
        width: '25ch',
      },
    },
    title:{
        width:"250px"
    }
  }));

const FormNewProduct = ({row, toggleDrawer, getAllStock}) =>{
    const classes = useStyles();

    const [product, setProduct] = React.useState({...row});

    const handleChange = (e, prop) =>{
        setProduct({
            ...product,
            [prop]: prop === 'status' ? e.target.checked : prop === 'productQuantity' ?  Number.parseInt( e.target.value) : e.target.value
        });
    }

    const saveProduct =  async (isNew, e) =>{
        
        if(isNew){
            let copia = {...product};
            delete copia._id;
            await Petition.insertProduct(copia)
            .then(res => {
                if(res.status == 201){
                    alert("Producto creado correctamente");
                    setProduct({...Product});
                    toggleDrawer(false, e);
                    getAllStock();
                }
            });
        }
        else{
            await Petition.updateProduct(product)
            .then(res => {
                console.log(res);
                if(res.status == 200){
                    alert("Producto actualizado correctamente");
                    setProduct({...Product});
                    setTimeout(() => {
                        toggleDrawer(false, e);
                        getAllStock();
                    }, 300);
                }
            });
        }
            
    }

    return (
        <form className={classes.root} noValidate autoComplete="off">
             <Typography variant="h6" component="h6" className={classes.title}>
                    {row.nameProduct == '' ? 'Nuevo Producto' : 'Editar producto'}
                </Typography>
            <TextField id="standard-basic" label="Nombre" variant="outlined" value={product?.nameProduct} onChange={(e)=> handleChange(e, 'nameProduct')}/> <br/>
            <FormControl variant="outlined">
                <InputLabel id="select-label">Categoria</InputLabel>
                <Select
                    id="Categoria"
                    value={product?.category}
                    onChange={(e)=> handleChange(e, 'category')}
                    >
                        <MenuItem value={'Bebidas'}>Bebidas</MenuItem>
                        <MenuItem value={'Limpieza'}>Limpieza</MenuItem>
                        <MenuItem value={'Botanas'}>Botanas</MenuItem>
                        <MenuItem value={'Cremeria'}>Cremeria</MenuItem>
                        <MenuItem value={'Panaderia'}>Panaderia</MenuItem>
                </Select>
            </FormControl><br/>
            <TextField id="outlined-basic" label="Descripcion" variant="outlined" value={product?.description} onChange={(e)=> handleChange(e, 'description')}/> <br/>
            <TextField id="filled-basic" type="number" label="Cantidad" variant="outlined" value={product?.productQuantity} onChange={(e)=> handleChange(e, 'productQuantity')}/> <br/>
            <div>
                <Checkbox
                    checked={product.status ?? false}
                    inputProps={{ 'aria-label': 'primary checkbox' }}
                    onChange={(e)=> handleChange(e, 'status')}
                />
                Activo
            </div>
            <Button
                variant="contained"
                color="primary"
                onClick={(e)=> saveProduct(row.nameProduct == '', e )}
                >
                    Guardar
                </Button>   
                
        </form>
    )
}
export default FormNewProduct;