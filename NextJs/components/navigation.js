import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Link from "next/link";

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
    linkMenu:{
        color: 'white',
        textDecoration: 'none'
    },
  }));

const Navigation = () =>{
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" className={classes.title}>
                        Store
                    </Typography>

                    <Link href="/">
                        <a className={classes.linkMenu}>
                            <Button color="inherit">Store</Button>
                        </a>
                    </Link>
                    
                    <Link href="/danielperez">
                        <a className={classes.linkMenu}>
                            <Button color="inherit">Candidato</Button>
                        </a>
                    </Link>
                </Toolbar>
            </AppBar>
        </div>
    )
}

export default Navigation;