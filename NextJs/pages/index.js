import React, { useEffect }  from "react";
import Wrap from "../components/wrap";
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import * as Petition from '../common/petitions-http';
import TablePaginationActions from '../components/TablepaginationActions';
import Box from '@material-ui/core/Box';
import Drawer from '@material-ui/core/Drawer';
import FormNewProduct from '../components/form-new';
import {Product} from '../common/product.class';

const useStyles = makeStyles({
    table: {
      minWidth: 500,
    },
  });

const Index = () =>{
    const classes = useStyles();
    const [rowSelect, setRowSelect] = React.useState({...Product});
    const [isNew, setIsNew] = React.useState(false);
    const [rows, setRows] = React.useState([]);
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
  
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);
  
    const handleChangePage = (event, newPage) => {
      setPage(newPage);
    };
  
    const handleChangeRowsPerPage = (event) => {
      setRowsPerPage(parseInt(event.target.value, 10));
      setPage(0);
    };

    const deleteProduct = async (row) => {
        if (window.confirm(`Desea eliminar el registro ${row.nameProduct}?`)) {
            await Petition.deleteProduct(row._id);
            await getAllStock();
        }
    }

    const edit = (row, e) =>{
        setRowSelect(row);
        setTimeout(() => {
            toggleDrawer(true, e)
        }, 300);
    }

    const getAllStock = async () =>{
        await Petition.getAllProductsStock()
        .then(data => {
            setRows([...data]);
        });
    }

    const newProduct= (opne, e) =>{
        setRowSelect({...Product});
        setTimeout(() => {
            toggleDrawer(true, e)    
        }, 200);
        
    }

    const toggleDrawer = (open, event)  => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
          return;
        }
        setIsNew(open);
      };

    useEffect(async ()=>{
        await getAllStock();
    },[])

    return (
        <Wrap title="Store Online">
            <Box my={2}>
                <Button
                    variant="contained"
                    color="primary"
                    onClick={(e) =>newProduct(true, e)}
                >
                    Nuevo Producto
                </Button>
            </Box>
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="custom pagination table">
                    <TableBody>
                    {(rowsPerPage > 0
                        ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        : rows
                    ).map((row) => (
                        <TableRow key={row._id}>
                            <TableCell component="th" scope="row">
                                { row.nameProduct}
                            </TableCell>
                            <TableCell style={{ width: 160 }} align="right">
                                { row.category}
                            </TableCell>
                            <TableCell style={{ width: 160 }} align="right">
                                {row.description}
                            </TableCell>
                            <TableCell style={{ width: 160 }} align="right">
                                {row.productQuantity}
                            </TableCell>
                            <TableCell style={{ width: 160 }} align="right">
                                {row.status ? 'activo' : 'no activo'}
                            </TableCell>
                            <TableCell style={{ width: 160 }} align="right">
                                {row.timeStamp}
                            </TableCell>   
                            <TableCell style={{ width: 160 }} align="right">
                                <IconButton color="primary" aria-label="edit" onClick={(e)=> edit(row, e)}>
                                    <EditIcon />
                                </IconButton>
                            </TableCell> 
                            <TableCell style={{ width: 160 }} align="right">
                                <IconButton color="secondary" aria-label="delete" style={{color:'red'}}  onClick={()=> deleteProduct(row)}>
                                    <DeleteIcon />
                                </IconButton>
                            </TableCell>                                                                      
                        </TableRow>
                    ))}

                    {emptyRows > 0 && (
                        <TableRow style={{ height: 53 * emptyRows }}>
                        <TableCell colSpan={6} />
                        </TableRow>
                    )}
                    </TableBody>
                    <TableFooter>
                        <TableRow>
                            <TablePagination
                                rowsPerPageOptions={[5, 10, 25, { label: 'Todo', value: -1 }]}
                                colSpan={8}
                                count={rows.length}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                SelectProps={{
                                    inputProps: { 'aria-label': 'registros por página' },
                                    native: true,
                                }}
                                onChangePage={handleChangePage}
                                onChangeRowsPerPage={handleChangeRowsPerPage}
                                ActionsComponent={TablePaginationActions}
                            />
                        </TableRow>
                    </TableFooter>
                </Table>
            </TableContainer>
            <Drawer anchor={'right'} open={isNew} onClose={(e) => toggleDrawer(false, e)}>
                <FormNewProduct row={rowSelect} toggleDrawer={toggleDrawer} getAllStock={getAllStock} />
            </Drawer>
        </Wrap>
    );
}

export default Index;