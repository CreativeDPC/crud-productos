import Wrap from "../components/wrap";
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      maxWidth: '100%',
      margin:'0 auto'
    },
    details: {
      display: 'flex',
      flexDirection: 'column',
    },
    content: {
      flex: '1 0 auto',
    },
    cover: {
      width: '50%',
    },
    description:{
        fontSize: '0.9em',
        marginBottom: '1em'
    }
  }));

const DanielPerez = () =>{
    const classes = useStyles();
    const theme = useTheme();
    return (
        <Wrap title="Daniel Pérez Cabrera">
            <Card className={classes.root}>
                <CardMedia
                    className={classes.cover}
                    image="https://scontent.flov1-1.fna.fbcdn.net/v/t1.0-9/103617753_2421600294609775_9084847350996394035_n.jpg?_nc_cat=105&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeH7LD0B_2mDKhvxxmGGdaOaVmw6HRQqjVFWbDodFCqNUToGk9ZY6lklnIxl9CXGvCtUXXx6go8Jmt6igXNv2NcI&_nc_ohc=yGIC8c9bmswAX-Ld9p3&_nc_ht=scontent.flov1-1.fna&oh=477c3d2828640cc6f4573f120b775928&oe=6038E1A7"
                    title="Danile Pérez Cabrera"
                />
                <div className={classes.details}>
                    <CardContent className={classes.content}>
                        <Typography component="h5" variant="h5">
                            Ingeniero en Computación
                        </Typography>
                        <Typography variant="subtitle1" color="textSecondary" className={classes.description}>
                            Soy un Desarrollador Web Full Stack con 8 años de experiencia.
                            Me considero una persona proactiva,
                            creativa, colaborativa y autodidacta. 
                        </Typography>
                        <Typography component="p" variant="p" >
                            Email: creativedesing.dpc@gmail.com <br />
                            Télefono: 55 1120 0160 <br />
                            Escuela: UNAM
                        </Typography>
                    </CardContent>
                </div>

            </Card>
        </Wrap>
    )
}

export default DanielPerez;