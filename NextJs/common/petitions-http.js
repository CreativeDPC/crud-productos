const server = 'http://localhost:3001';

export const getAllProductsStock = async () =>{
    return fetch(`${server}/products/stock`)
    .then(r => r.json());        
}

export const getAllProductsEmpty = async () =>{
    return await fetch(`${server}/products/empty`)
    .then(r => r.json());        
}

export const insertProduct = async (row) =>{
    return await fetch(`${server}/products`,{
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
        body: JSON.stringify(row),
        
    });        
}

export const updateProduct = async (row) =>{
    return await fetch(`${server}/products/${row._id}`,{
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
        body: JSON.stringify(row)
    });        
}

export const deleteProduct = async (id) =>{
    return fetch(`${server}/products/${id}`,{
        method: 'DELETE'
    })
    .then(r => r.json());        
}


